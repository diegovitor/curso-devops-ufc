#!/bin/bash
# docker-compose ps --status=running app1
# $(docker ps -q -f status=running)
PROJECT_NAME="cluster-docker" 

if [ ! "$(docker compose ls --format json | jq -r '.[].Name')" == $PROJECT_NAME ]; 
then
  echo "RODAR DOCKER COMPOSE"
#   docker compose -p $PROJECT_NAME up -d
  docker compose -p cluster-docker up -d 
  #--scale meuhost=2
fi

CONTAINER_NAME='ansible-controller'
CONTAINER_ID=$(docker ps -q -f status=running -f name=^/${CONTAINER_NAME}$)
if [ ! "${CONTAINER_ID}" ]; then
  echo "CONTAINER NÃO EXISTE"
fi

docker ps -q -f status=running
docker exec -u devops -it $CONTAINER_ID /bin/bash

#unset CONTAINER_ID