#!/bin/bash

# ssh-keygen -t ed25519 -f /home/devops/key
# ssh-keygen -b 4096 -t rsa -f /home/devops/.ssh/key -N ""

# para copiar as chaves para outros hosts
#ssh-copy-id -i /home/devops/.ssh/key devops@debian01

# gerar as chaves no diretório /data/ssh

ssh-keygen -b 4096 -t rsa -C devops@ansible-controller -f ./docker/data/ssh/key -N ""