#!/bin/bash
source .env

mkdir -p $DIR_ARTEFATOS/atividade06
cd $DIR_ARTEFATOS/atividade06

echo -e "\n 6.1 - Gerar Docker Compose"
cat << EOF > docker-compose.yml
version: '3.8'
services:
  db:
    # We use a mariadb image which supports both amd64 & arm64 architecture
    image: mariadb:10.6.4-focal
    # If you really want to use MySQL, uncomment the following line
    #image: mysql:8.0.27
    command: '--default-authentication-plugin=mysql_native_password'
    volumes:
      - db_data:/var/lib/mysql
    restart: always
    environment:
      - MYSQL_ROOT_PASSWORD=somewordpress
      - MYSQL_DATABASE=wordpress
      - MYSQL_USER=wordpress
      - MYSQL_PASSWORD=wordpress
    expose:
      - 3306
      - 33060
  wordpress:
    image: wordpress:latest
    ports:
      - 8082:80
    restart: always
    environment:
      - WORDPRESS_DB_HOST=db
      - WORDPRESS_DB_USER=wordpress
      - WORDPRESS_DB_PASSWORD=wordpress
      - WORDPRESS_DB_NAME=wordpress
volumes:
  db_data:
EOF

echo -e "\n 6.2 - Executar Docker Compose"
docker compose -p atividade6 up -d
docker compose ls

echo -e "\n 6.3 - Mostrar conteúdo da página http://localhost:8082"
curl -L -I  http://localhost:8082