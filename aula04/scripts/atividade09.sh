#!/bin/bash
source .env

echo -e "\n9.1 - Baixar a imagem do registry dockerhub"
docker pull $DOCKER_IMAGE

echo -e "\n9.2 - Executar o container baixado"
# Inclui o comando top para manter o container up
docker run -itd $DOCKER_IMAGE top