#!/bin/bash
source .env
mkdir -p $DIR_ARTEFATOS/atividade08
cd $DIR_ARTEFATOS/atividade08

echo -e "\n 8.2 - logar no registry dockerhub"
docker login -u $DOCKERHUB_USER -p $DOCKERHUB_TOKEN docker.io

echo -e "\n 8.3 - Criar arquivo Dockerfile"
cat << EOF > Dockerfile
FROM ubuntu:latest
RUN apt-get update && apt-get install -y curl
CMD ["curl", "https://gitlab.com"]
EOF

echo -e "\n 8.4 - Build imagem Docker \n"
docker build -t $DOCKER_IMAGE .

# inclui o comando top para manter um processo principal executando no container ubuntu
docker run -itd --rm $DOCKER_IMAGE top
docker ps

echo -e "\n 8.5 - Subir imagem para o registry do dockerhub \n"
docker push $DOCKER_IMAGE

