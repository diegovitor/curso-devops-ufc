#!/bin/bash
# atividade 5.1 a 5.3
source .env

mkdir -p $DIR_ARTEFATOS/atividade05
cd $DIR_ARTEFATOS/atividade05

echo -e "\n 5.1 - Gerar Docker Compose"
cat << EOF > docker-compose.yml
version: '3.8'
services:
  web:
    image: nginx
    ports:
      - "8081:80"
  db:
    image: postgres
    environment:
      - POSTGRES_PASSWORD="mysecretpassword"
EOF

echo -e "\n 5.2 - Executar Docker Compose"
docker compose -p atividade5 up -d
docker compose ls

echo -e "\n 5.3 - Mostrar conteúdo da página http://localhost"

curl -L -I http://localhost:8081