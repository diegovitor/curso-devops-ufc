#!/bin/bash

source .env
DIR_ATIV_01_04=$DIR_ARTEFATOS/atividade01-04

# atividade 1.1-1.3
echo "1.2 - Gerar Dockerfile \n"
docker run hello-world

# atividade 2.1 - 2.4
echo "2.1 - Criar diretorio"
mkdir -p $DIR_ATIV_01_04/minha-imagem
cd $DIR_ATIV_01_04/minha-imagem

echo "2.2 - Gerar Dockerfile"
cat << EOF > Dockerfile
FROM ubuntu:latest
RUN apt-get update && apt-get install -y curl
CMD ["curl", "https://gitlab.com"]
EOF

echo "2.3 - Build imagem Docker \n"
docker build -t meudocker .

echo "2.4 - Run meudocker \n"
docker run meudocker

# atividade 3.1 a 3.3
echo "\n 3.1 - Criar diretório para armazenar os dados do container"
mkdir -p $DIR_ATIV_01_04/dados-container

echo "3.2 e 3.3 - Vincular o diretório do host ao volume do container e listar o conteúdo do volume \n"
docker run -it --rm \
-v $DIR_ATIV_01_04/dados-container:/dados-container \
-d --name meudocker ubuntu:latest 

ls -lsa $DIR_ATIV_01_04/dados-container

# atividade 4.1 a 4.3
echo "\n 4.1 - Criar rede minha-rede"
docker network create minha-rede

echo "\n 4.2 - Criar 2 containers cont1 e cont2"
docker run --network minha-rede --name cont1 -d nginx:alpine
docker run --network minha-rede --name cont2 -d nginx:alpine

echo "\n 4.3 - Executar comando ping do cont1 para o cont2"
# manual
#docker exec -it cont1 bash
#ping cont2

# via shell script
docker exec -it cont1 ping -w 3 cont2


 