#!/bin/bash
source .env

echo -e "\n 7.1 - Criar diretório compose_flask"
mkdir -p $DIR_ARTEFATOS/atividade07/compose_flask
cd $DIR_ARTEFATOS/atividade07/compose_flask

echo -e "\n 7.2 - Criar arquivo app.py"
cat << EOF > app.py
import time
import redis

from flask import Flask
app = Flask(__name__)
cache = redis.Redis(host='redis', port=6379)

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
                retries -= 1
                time.sleep(0.5)

@app.route('/')
def hello():
    count = get_hit_count()
    return 'Hello World! I have been seen {} times.\n'.format(count)
EOF

echo -e "\n 7.3 - Criar arquivo requirements.txt"
cat << EOF > requirements.txt
flask
redis
EOF

echo -e "\n 7.4 - Criar arquivo Dockerfile"
cat << EOF > Dockerfile
# syntax=docker/dockerfile:1
FROM python:3.7-alpine
WORKDIR /code
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 5000
COPY . .
CMD ["flask", "run"]
EOF

echo -e "\n 7.5 - Criar arquivo docker-compose.yml"
cat << EOF > docker-compose.yml
# retirado de https://docs.docker.com/compose/gettingstarted/
services:
  web:
    build: .
    ports:
      - "8000:5000"
  redis:
    image: "redis:alpine"
EOF

echo -e "\n 7.6 - Executar Docker Compose"
docker compose -p atividade7 up -d
docker compose ls

echo -e "\n 7.7 - Mostrar conteúdo da página http://localhost:8000"
curl -L http://localhost:8000