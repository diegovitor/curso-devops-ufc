# Curso-Devops-UFC

# Requisitos
 - Vagrant 2.2+
 - Virtualbox 6.1+
 - mínimo de 4GB de RAM

## Ambiente de simulação
Para criar o ambiente de simulação é necessário possuir o vagrant instalado. 

```bash 
$ vagrant up
```

após a instalação, o seguinte comando poderá ser executado para verificar o status 
das VM criadas:

```bash 
$ vagrant status
```

Caso tudo tenha ocorrido direito, o terminal mostrará os seguintes hosts criados

```bash
Current machine states:

vm01                      running (virtualbox)

This environment represents multiple VMs. The VMs are all listed
above with their current state. For more information about a specific
VM, run `vagrant status NAME`.
```

o vagrant por padrão cria um usuário vagrant juntamente com o par de chaves ssh.

Para acessar a VM, basta executar o seguinte comando:

```bash
vagrant ssh $hostname
```
ex. 
vagrant ssh vm01
# Aula04

## Estrutura do projeto
 ```aula04``` - Todas as informações do projeto.
 ```aula04/scripts``` - Scripts para execução das atividades
 ```aula04/scripts/clean_aula04.sh``` - Script criado para limpar o ambiente, remover os containers, volumes e networks
 ```aula04/artefatos``` - Artefatos gerados pelos scripts
 ```aula04/Atividade_2_-_Docker.pdf``` - [Atividade02 - Docker](./aula04/Atividade_2_-_Docker%20(1).pdf)

## Executar a atividade da aula04

  1. Acessar a VM01
  ```
  vagrant ssh vm01
  ```
  2. Acessar o diretório compartilhado
  ```
  cd /mnt/vagrant/aula04/scripts
  ```
  3. Executar o script aula04.sh

```
OBS1: Foi criado o arquivo scripts/.env para armazenar variáveis locais.
OBS2: Alterar a variável DOCKERHUB_USER
OBS3: A variável DOCKERHUB_TOKEN não deve ser definida no arquivo .env 
```

# Aula05

## Executar a atividade da aula05 - atividade03

## Estrutura do projeto
 ```aula05``` - Todas as informações referente a aula05.
 ```aula05/artefatos``` - Diretório que armazena todos os artefatos criados para executar a atividade03.
 ```aula05/artefatos/ansible``` -  Diretório que armazena o playbook, inventário e as roles
 ```aula05/artefatos/docker``` - Diretório que armazena o dockerfile e arquivos de dados utilizado nos containers
 ```aula05/artefatos/docker/data/ssh``` - Chaves ssh. A chave pública é copiada para .ssh dos containers
 ```aula05/Atividade_3_-_ansible.pdf``` - [Atividade03 - Docker](./aula05/Atividade_3_-_ansible.pdf)

## Comandos para iniciar o laboratório 
1. - Acessar a VM01 vagrant e mudar o diretório para /mnt/vagrant/aula05/artefatos/
2. - Executar o ./deploy.sh para subir os containers
3. - Antes de executar o comando ansible, é necessário pegar os ips dos containers executados no docker-compose e inseri-los no 
arquivo de inventário do ansible. 
    i.  O seguinte comando pode ser executado para inspecionar a network ativiade03
      ```
      docker inspect atividade03
      ```
4. - Acessar a parta /ansible e executar os comandos abaixo para executar o playbook.yml
5. - Para cada questão foi criada uma role que pode ser executado alterando o playbook conforme abaixo
```
  roles:
    - name: roles/questao01
      tags: ['questao01']
```

### Comando ansible para montar o ambiente
Após criada as VM, executar o comando abaixo para montar o ambiente

```bash
ansible-playbook ./playbook.yml -u devops -i ./inventory.ini
```
comando para teste de ping no grupo hosts 
```bash 
ansible -i inventory.ini -m ping hosts
``````



